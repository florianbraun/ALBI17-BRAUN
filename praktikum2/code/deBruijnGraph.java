package contigs.search;

import java.io.*;
import java.util.*;

public class deBruijnGraph {

	Map<String, Knoten> dictionary = new HashMap<String, Knoten>();
	
	public deBruijnGraph(String strPath, int intMer){
		
		File file = new File("C:\\Users\\Flo\\Desktop\\5.Semester\\Algorithmische Bioinformatik\\Praktikum\\2.Projektiteration\\n_deltocephalinicola__reads__100.txt");
		BufferedReader buffRead = null;
		try {
			buffRead = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//get all kMers into an Array
		String strTxt;
		Knoten knotTemp;
		Knoten knotTempSucc;
		String kMer;
		String kMerLeft;
		String kMerRight;
		
		try {
			//get Fileinput
			strTxt = buffRead.readLine();
			while((strTxt = buffRead.readLine()) != null){
				for(int i=0; i< 100-intMer+1; i++){	
					kMer = strTxt.substring(i,i+intMer);
					kMerLeft = kMer.substring(0, intMer);
					kMerRight = kMer.substring(1);
					knotTemp = this.dictionary.get(kMerLeft);
					knotTempSucc = this.dictionary.get(kMerRight);
					
					//check if there is already a Knoten and successor Knoten with the same name, if not initialize him
					if(knotTemp == null){
						knotTemp = new Knoten(kMerLeft);
						this.dictionary.put(kMerLeft, knotTemp);
					}
					if(knotTempSucc == null){
						knotTempSucc = new Knoten(kMerRight);
						this.dictionary.put(kMerRight, knotTempSucc);
					}
					
					//save the successor in the arraylist and increment out of the Knot and in of the successor
					knotTemp.successors.add(kMerRight);
					knotTemp.out++;
					knotTempSucc.in++;				
				}
				
					
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void printContigs(deBruijnGraph dBG){
		
		ArrayList<String> contigs = new ArrayList<>();
		int kMerCount = dBG.dictionary.size();
		Set<String> keys = dictionary.keySet();
		Iterator<String> iterate = keys.iterator();
		Knoten temp = null;
		Knoten knoIter;
		String strTemp;
		int Test = 0;
		String strIter;
		
		//start MaximalNonBranchingGraph
		for(int i = 0; i<kMerCount; i++){
			strTemp = iterate.next();
			temp = dBG.dictionary.get(strTemp);
			if(!(temp.in == 0 && temp.out == 0)){
				if(temp.out > 0){
					for(int j = 0; j < temp.out; j++){
						strTemp = temp.strName + temp.successors.get(0).substring(temp.successors.get(0).length());
						knoIter = dBG.dictionary.get(temp.successors.get(0));
						while(knoIter.in == 1 && knoIter.out == 1){
							strTemp = strTemp + knoIter.successors.get(0).substring(knoIter.successors.get(0).length());
							knoIter = dBG.dictionary.get(knoIter.successors.get(0));
						
						}
						contigs.add(strTemp);
					}
				}
			}
			
			
			
			
		}
		System.out.println(contigs.size());
		
		
	}
}

