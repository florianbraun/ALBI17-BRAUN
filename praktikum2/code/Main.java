package contigs.search;

import java.io.*;
public class Main {
	
	public static void main(String[] args) throws Exception{
		
		String strFile = "C:\\Users\\Flo\\Desktop\\5.Semester\\Algorithmische Bioinformatik\\Praktikum\\2.Projektiteration\\n_deltocephalinicola__reads__100.txt";
		int kMer = 99;
		deBruijnGraph dBG = new deBruijnGraph(strFile, kMer);
		dBG.printContigs(dBG);
		System.out.println(dBG.dictionary.size());
	}

}
